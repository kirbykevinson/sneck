using System;
using System.Collections.Generic;
using System.Linq;

namespace sneck {
	enum SDirection {
		Up,
		Down,
		Left,
		Right
	}
	
	class Game {
		Random Rand = new Random();
		
		int Score = 0;
		
		int Width = 0;
		int Height = 0;
		
		const ConsoleColor BorderColor = ConsoleColor.Red;
		const ConsoleColor FieldColor = ConsoleColor.Blue;
		const ConsoleColor SnakeColor = ConsoleColor.White;
		const ConsoleColor AppleColor = ConsoleColor.Green;
		
		SDirection Direction = SDirection.Up;
		
		List<(int, int)> Snake = new List<(int, int)>();
		(int, int) Apple = (0, 0);
		
		public void Run() {
			DrawField();
			SpawnApple();
			
			long lastRender = GetTimeMs();
			
			while (true) {
				GetDirection();
				
				if (lastRender != GetTimeMs() && GetTimeMs() % 100 == 0) {
					Move();
					
					lastRender = GetTimeMs();
				}
			}
		}
		
		void DrawPixel(int x, int y, ConsoleColor color) {
			Console.SetCursorPosition(x * 2, y);
			
			Console.BackgroundColor = color;
			
			Console.Write("  ");
			
			Console.SetCursorPosition(Width * 2, Height);
		}
		
		long GetTimeMs() {
			return DateTime.Now.Ticks / 10000;
		}
		
		void DrawField() {
			Width = Console.WindowWidth / 2;
			Height = Console.WindowHeight;
			
			Snake.Add((Width / 2, Height / 2));
			
			for (var y = 0; y < Height; y++) {
				for (var x = 0; x < Width; x++) {
					var color = FieldColor;
					
					if (x == 0 || y == 0 || x == Width - 1 || y == Height - 1) {
						color = BorderColor;
					}
					
					DrawPixel(x, y, color);
				}
			}
			
			DrawPixel(Snake[0].Item1, Snake[0].Item2, SnakeColor);
		}
		
		void SpawnApple() {
			do {
				Apple = (
					Rand.Next(1, Width - 1),
					Rand.Next(1, Height - 1)
				);
			} while (Snake.Contains(Apple));
			
			DrawPixel(Apple.Item1, Apple.Item2, AppleColor);
		}
		
		void GetDirection() {
			if (!Console.KeyAvailable) {
				return;
			}
			
			ConsoleKey key = Console.ReadKey().Key;
			
			switch (key) {
			case ConsoleKey.UpArrow:
				Direction = SDirection.Up;
				
				break;
			case ConsoleKey.DownArrow:
				Direction = SDirection.Down;
				
				break;
			case ConsoleKey.LeftArrow:
				Direction = SDirection.Left;
				
				break;
			case ConsoleKey.RightArrow:
				Direction = SDirection.Right;
				
				break;
			}
		}
		
		void Move() {
			var tail = Snake[0];
			var head = Snake.Last();
			
			switch (Direction) {
			case SDirection.Up:
				head = (head.Item1, head.Item2 - 1);
				
				break;
			case SDirection.Down:
				head = (head.Item1, head.Item2 + 1);
				
				break;
			case SDirection.Left:
				head = (head.Item1 - 1, head.Item2);
				
				break;
			case SDirection.Right:
				head = (head.Item1 + 1, head.Item2);
				
				break;
			}
			
			if (
				head.Item1 == 0 ||
				head.Item1 == Width - 1 ||
				head.Item2 == 0 ||
				head.Item2 == Height - 1 ||
				Snake.Contains(head)
			) {
				Die();
			}
			
			if (head == Apple) {
				Score++;
				
				SpawnApple();
			} else {
				DrawPixel(tail.Item1, tail.Item2, FieldColor);
				Snake.RemoveAt(0);
			}
			
			DrawPixel(head.Item1, head.Item2, SnakeColor);
			Snake.Add(head);
		}
		
		void Die() {
			Console.ResetColor();
			Console.Clear();
			
			Console.WriteLine($"You died. Your score is {Score}.");
			
			Environment.Exit(0);
		}
	}
	
	class Program {
		static void Main(string[] args) {
			var game = new Game();
			
			game.Run();
		}
	}
}
